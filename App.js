
import React from 'react'
import { View, Text,Image } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './src/Screens/LoginPage/Login';
import LoginSign from './src/Screens/Login/LoginSign';
import Signup from './src/Screens/SignUp/Signup';
import Home from './src/Screens/HomePage/Home';
import { Images } from './src/utils/Constants';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Category from './src/Screens/Categories/Category';
import SubCategroy from './src/Screens/SubCategory/SubCategroy';
import Search from './src/Screens/SearchPage/Search';
import Account from './src/Screens/AccountPage/Account';
import SetImage from './src/Screens/AccImagePage/SetImage';
import EditProfile from './src/Screens/AccEditProfile/EditProfile';
import InvoicesBilling from './src/Screens/Invoices & Billing/InvoicesBilling';
import Setting from './src/Screens/SettingPage/Setting';
import HelpSupport from './src/Screens/Help&SupportPage/HelpSupport';
import ImageShow from './src/Screens/ImageShowPage/ImageShow';
import CreatePassword from './src/Screens/CreatePassword/CreatePassword';
import WhatOffer from './src/Screens/WhatOfferingPage/WhatOffer';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const getTabView = () => {
    
  return (
  
    <Tab.Navigator
      screenOptions={{
        headerShown: false
      }}
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          bottom: 25,
          left: 20,
          right: 20,

        }
      }}
    
    > 

      <Tab.Screen
        name='Home' component={Home} options={{
          tabBarIcon: ({ focused }) => (

            <View style={{flex:0, alignItems: 'center', justifyContent: 'center' }}>

              <Image style={{height:30,width:30,
                           tintColor: focused ? '#022A2A' : 'lightgrey'}}
              source={Images.Home_Icon}
                resizeMode='contain'
                
              />
              <Text style={{
                color: focused ? '#022A2A' : 'black',
                fontSize: 12,
                borderBottomWidth: 1,
                borderBottomColor: focused ? '#022A2A' : 'lightgrey'
              }}>
                Home
              </Text>

            </View>
          ),
        }}
      />
      <Tab.Screen name='LoginSign' component={LoginSign}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{flex:0, alignItems: 'center', justifyContent: 'center', }}>
              <Image style={{height:30,width:30,
                            tintColor: focused ? '#022A2A' : 'lightgrey'}}
              source={Images.Chat_Icon}
                resizeMode='contain'
                
              />
              <Text style={{
                color: focused ? '#022A2A' : 'black', fontSize: 12,
                borderBottomWidth: 1,
                borderBottomColor: focused ?'#022A2A' : 'lightgrey'
              }}>
                Chat
              </Text>

            </View>
          ),
        }}
      />

<Tab.Screen name='WhatOfffer' component={WhatOffer}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{flex:0, alignItems: 'center', justifyContent: 'center',
             marginTop: 6,bottom:18,}}>
              <Image style={{height:60,width:60, borderRadius:100 
              }} 
              source={Images.Sell_Icon}

                resizeMode='contain'
              />
              <Text style={{
                color: focused ? '#022A2A' : 'black',
                fontSize: 12 ,
                 marginTop: 10,
                borderBottomWidth: 1,
                bottom:5,
                borderBottomColor: focused ? '#022A2A' : 'lightgrey'
              }}>
                SELL
              </Text>
            </View>
          ),
        }}
      />


      <Tab.Screen name='Category' component={Category}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{flex:0, alignItems: 'center', justifyContent: 'center', marginTop: 6 }}>
              <Image style={{height:30,width:30,
              tintColor: focused ? '#022A2A' : 'lightgrey'}} 
              source={Images.Heart_Icon}

                resizeMode='contain'
               
              />
              <Text style={{
                color: focused ? '#022A2A' : 'black',
                fontSize: 12 ,
                 marginTop: 4,
                borderBottomWidth: 1,
                bottom:5,
                borderBottomColor: focused ? '#022A2A' : 'lightgrey'
              }}>
                Heart
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen name='Account' component={Account}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{flex:0, alignItems: 'center', justifyContent: 'center' }}>
              <Image style={{height:30,width:30,
                           tintColor: focused ? '#022A2A' : 'lightgrey'}}
               source={Images.Account_Icon}

                resizeMode='contain'
               
              />
              <Text style={{
                color: focused ? '#022A2A' : 'black',
                fontSize: 12,
                borderBottomWidth: 1,
                borderBottomColor: focused ? '#022A2A' : 'lightgrey'
              }}>
                Account
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  )
}


export default function App() {

  return (
    <NavigationContainer>
    <Stack.Navigator
     screenOptions={{
          headerShown: false
        }}>
      <Stack.Screen name="Login" component={Login} />
       <Stack.Screen name="LoginSign" component={LoginSign} />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="Home" component={getTabView} />
      <Stack.Screen name="Category" component={Category} />
      <Stack.Screen name="SubCategory" component={SubCategroy} />
      <Stack.Screen name="Search" component={Search} />
      <Stack.Screen name="Account" component={Account} />
      <Stack.Screen name="SetImage" component={SetImage} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="InvoicesBilling" component={InvoicesBilling} />
      <Stack.Screen name="Setting" component={Setting} />
      <Stack.Screen name="HelpSupport" component={HelpSupport} />
      <Stack.Screen name="ImageShow" component={ImageShow} />
      <Stack.Screen name="CreatePassword" component={CreatePassword}/>
      <Stack.Screen name="WhatOffer" component={WhatOffer} />
    
    
    
    </Stack.Navigator>
  </NavigationContainer>
  )
}

