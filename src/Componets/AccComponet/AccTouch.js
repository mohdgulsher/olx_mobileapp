import React from 'react'
import { View, Text,TouchableOpacity,Image } from 'react-native'
import StyleAccTouch from './StyleAccTouch'

export default function AccTouch(props) {
    return (
        <View>
            <View style={StyleAccTouch.mainView}>
            <TouchableOpacity onPress={props.onPress}
            style={{flexDirection:'row'}}>
                <Image  style={StyleAccTouch.image1}
                source={props.Image1}/>
                <View style={{marginLeft:15,}}>
                    <Text style={StyleAccTouch.text1}>
                      {props.Text1}
                    </Text>
                    <Text style={StyleAccTouch.text2}>
                    {props.Text2}
                    </Text>
                </View>
                <View style={StyleAccTouch.image2}>
                    <Image source={props.Image2}/>
                </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

