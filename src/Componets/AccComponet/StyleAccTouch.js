import { StyleSheet } from "react-native";

export default StyleSheet.create({
    mainView:{
        marginTop:15,marginLeft:10,marginRight:10,
            borderBottomWidth:1,borderBottomColor:'lightgrey',
            paddingBottom:15
    },
    image1:{
           height:35,width:35,
           alignSelf:'center'
    },
    text1:{
        fontSize:16,color:'#022A2A'
    },
    text2:{
        fontSize:14,color:'#022A2A'
    },
    image2:{
    position:'absolute',right:10,alignSelf:'center'
    }
})