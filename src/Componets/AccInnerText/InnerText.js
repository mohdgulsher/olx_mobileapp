
import React from 'react'
import { View, Text,TouchableOpacity,Image } from 'react-native'
import StyleInnerText from './StyleInnerText'


export default function InnerText(props) {
    return (
        <View>
            <View style={StyleInnerText.mainView}>
            <TouchableOpacity onPress={props.onPress}
            style={{flexDirection:'row'}}>
                <View style={{marginLeft:15,}}>
                    <Text style={StyleInnerText.text1}>
                      {props.Text1}
                    </Text>
                    <Text style={StyleInnerText.text2}>
                    {props.Text2}
                    </Text>
                </View>
                <View style={StyleInnerText.image}>
                    <Image source={props.Image}/>
                </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

