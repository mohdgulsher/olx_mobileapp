import { StyleSheet } from "react-native";

export default StyleSheet.create({
    mainView:{
        marginTop:15,
            borderBottomWidth:1,borderBottomColor:'lightgrey',
            paddingBottom:15
    },
   
    text1:{
        fontSize:16,color:'#022A2A',
        marginLeft:10,marginRight:20,
        fontWeight:'bold'
    },
    text2:{
        fontSize:13,color:'#022A2A',
        marginLeft:10,marginRight:20
    },
    image:{
    position:'absolute',right:10,alignSelf:'center'
    }
})