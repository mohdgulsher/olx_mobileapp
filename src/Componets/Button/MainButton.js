import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import StyleButton from './StyleButton'

export default function MainButton(props) {
    return (
        <TouchableOpacity onPress={props.onPress} 
        style={[StyleButton.toucableOpacity, props.style]} >

            <Text style={StyleButton.textLogin} >{props.title}</Text>

        </TouchableOpacity>
    )
}
