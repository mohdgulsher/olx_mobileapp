import React,{createRef} from 'react'
import { View, Text,Image,TouchableOpacity } from 'react-native';
import {
    TextField,
    FilledTextField,
    OutlinedTextField
  } from 'rn-material-ui-textfield'
import { Images } from '../../utils/Constants';
import StyleFilled from './StyleFilled';

export default function FilledText(props) {


    
    return (
        <View style={[StyleFilled.containerView,props.style1]}>
        <View style={{flex:1}}>
             <TextField
        //  inputContainerStyle={{
              
        //     }}
            labelFontSize={16}
            fontSize={14}
            textColor='#022A2A'
            baseColor='#022A2A'
            tintColor={props.tintColor}
            multiline={props.multiline}
            label={props.label}
            keyboardType={props.keyboardType}
            lineWidth={props.lineWidth}
            activeLineWidth={props.activeLineWidth}
            disabledLineWidth={0}
          value={props.value}
          maxLength={props.maxLength}
          onChangeText={props.onChangeText}
          secureTextEntry={props.secureTextEntry}
        />
        </View>
        <TouchableOpacity onPress={props.onPress}
         style={StyleFilled.Touchable}>
        <Image style={StyleFilled.image}
         source={props.imageText} />
         </TouchableOpacity>


         
        </View>
    )
}
