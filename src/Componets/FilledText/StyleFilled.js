import { StyleSheet } from "react-native";

export default StyleSheet.create({
    containerView:{
        flexDirection:'row',backgroundColor:'white',
        borderBottomWidth:1,borderBottomColor:'#022A2A',
         marginLeft: 23, marginRight: 23
    },
    Touchable:{
        flex:0,
         marginBottom:25,
       alignSelf:'flex-end',
        marginRight:1
    },
    image:{
        flex:0,
        marginLeft:10,
       
    }
})