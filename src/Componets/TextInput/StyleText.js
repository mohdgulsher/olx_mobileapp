import { StyleSheet } from "react-native";

export default StyleSheet.create({
    mainView:{
        flex:0,
        marginTop:5
    },
    Touchable:{
        flexDirection:'row',
        marginTop:4,
        marginLeft:30,
        marginRight:35,
        padding:13,
        borderRadius:8,
        backgroundColor:'white'
    },

    image:{
        marginLeft:10,alignSelf:'center',
        width:20,
        height:20

    },
    text:{
        color:"black",
        fontSize:16,
            marginLeft:10,
            alignItems:'center',
            justifyContent:'center'
    }

})
