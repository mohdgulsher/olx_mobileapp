import React from 'react'
import { View, Text, TextInput,TouchableOpacity,Image } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleText from './StyleText'

export default function TextComponent(props) {
    return (
        <View style={StyleText.mainView}>
        <TouchableOpacity  style={StyleText.Touchable}
        onPress={props.onPress}>
        <Image  style={StyleText.image}
        source={props.image}/> 

            <Text style={StyleText.text}>{props.text}</Text>

           
            </TouchableOpacity>
        </View>
    )
}
