import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleEditProfile from './StyleEditProfile';
import FilledText from '../../Componets/FilledText/FilledText';
import { useRoute } from '@react-navigation/native';
import Modal from "react-native-modal";
import ImagePicker from 'react-native-image-crop-picker';

var FormData = require('form-data');

export default function EditProfile(props) {

    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleModal = () => setIsModalVisible(() => !isModalVisible);

    const route = useRoute();
    const [name, setName] = useState(`${props.route.params.getvaluename}`)
    const [someabout, setSomeabout] = useState('')
    const [mobile, setMobile] = useState('')
    const [email, setEmail] = useState(`${props.route.params.getvalueemail}`)

    const [cameragallery, setCameraGallery] = useState('https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg')


    const onSubmit = () => {
        route.params.Clickhandle(name)
        route.params.Clickhandle1(someabout)
        route.params.Clickhandle2(mobile)
        route.params.Clickhandle3(email)
        props.navigation.goBack();
    }

    const GalleryCamera = () => {
        setIsModalVisible(false)
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            console.log(image);
            setCameraGallery(image.path);
            // var form = new FormData()
            // form.append('image', {
            //     uri: image.path,
            //     type: image.mime,
            //     name: "Image",
            // })
           
        });

    }

    const GalleryImage = () => {
        setIsModalVisible(false)
        ImagePicker.openPicker({
            // multiple: true
        }).then(image => {
            console.log(image);
            setCameraGallery(image.path);
            // fdata.append('image',{
            //     uri:image.path,
            //       type: image.mime,
            //       name: "Image",
            //   })
           


        });
    }




    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={StyleEditProfile.headerView}>
                <View style={StyleEditProfile.headerImageView1}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack()
                        }}
                        style={StyleEditProfile.headerTouchable1} >
                        <Image
                            source={Images.Cross_Icon} />
                    </TouchableOpacity>
                </View>
                <View style={StyleEditProfile.headerImageView2}>
                    <TouchableOpacity
                        onPress={onSubmit}
                    >
                        <Text
                            style={StyleEditProfile.headerText}
                        >Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView>
                <Text style={{
                    fontSize: 18, marginLeft: 20, marginTop: 25, color: '#022A2A',
                    fontWeight: 'bold'
                }}>
                    Basic information
                </Text>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>

                    <TouchableOpacity
                        onPress={handleModal}
                        style={{ marginLeft: 10, borderRadius: 100 }}>
                        <Image style={{ width: 100, height: 100, borderRadius: 100 }}
                            source={{ uri: cameragallery }}
                        // {Images.Acc_Image}1


                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1 }}>
                        <FilledText
                            keyboardType="text"
                            label="Enter your name"
                            lineWidth={1}
                            activeLineWidth={1}
                            tintColor="aqua"
                            style1={{ borderBottomWidth: 0 }}
                            value={name}
                            onChangeText={(name) => setName(name)}
                        />
                    </View>

                </View>
                <View style={{
                    paddingBottom: 25, borderBottomWidth: 1,
                    borderColor: 'lightgrey'
                }}>
                    <FilledText
                        keyboardType='text'
                        maxLength={40}
                        lineWidth={1}
                        activeLineWidth={1}
                        tintColor="aqua"
                        label="Something about you "
                        style1={{ borderBottomWidth: 0 }}
                        value={someabout}
                        onChangeText={(someabout) => setSomeabout(someabout)}
                    />
                </View>

                <Text style={{
                    marginTop: 40, fontSize: 18, fontWeight: 'bold',
                    marginLeft: 20, color: '#022A2A'
                }}>
                    Contact information
                </Text>

                <FilledText
                    label="Phone Number"
                    keyboardType="phone-pad"
                    maxLength={10}
                    lineWidth={1}
                    activeLineWidth={1}
                    tintColor="aqua"
                    style1={{ borderBottomWidth: 0 }}
                    value={mobile}
                    onChangeText={(mobile) => setMobile(mobile)}
                />

                <View style={{
                    marginTop: 10, backgroundColor: 'yellow', marginLeft: 20,
                    marginRight: 20, paddingBottom: 10,
                }}>
                    <Text style={{
                        fontSize: 14, marginLeft: 10, marginRight: 10,
                        alignItems: 'center', justifyContent: 'center', color: '#022A2A'
                    }}>
                        You need to verify this number.Verifed numbers
                        let us reach you.
                        <TouchableOpacity style={{ marginTop: 30 }}>
                            <Text style={{
                                borderBottomWidth: 1, color: '#022A2A',
                                borderBottomColor: '#022A2A', fontSize: 14, fontWeight: 'bold'
                            }}>Verify it now </Text>
                        </TouchableOpacity>
                    </Text>

                </View>

                <View style={{
                    paddingBottom: 15, borderBottomColor: 'lightgrey',
                    borderBottomWidth: 1
                }}>

                    <FilledText
                        label="Email"
                        keyboardType="text"
                        maxLength={30}
                        lineWidth={1}
                        activeLineWidth={1}
                        tintColor="aqua"

                        style1={{ borderBottomWidth: 0 }}
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                    <Text style={{
                        marginLeft: 20, marginTop: 5, fontSize: 13, marginRight: 30,
                        color: '#022A2A'
                    }}>
                        Your email is never shared with external parties nor
                        do we use it to spam you in any way.
                    </Text>
                </View>
                <Text style={{
                    marginLeft: 20, marginTop: 10, fontSize: 18, marginRight: 20,
                    color: '#022A2A', fontWeight: 'bold'
                }}>
                    Additional information
                </Text>
                <Text style={{
                    marginLeft: 20, fontSize: 16, color: '#022A2A',
                    marginTop: 10
                }}>
                    Facebook
                </Text>
                <View style={{
                    marginLeft: 20, flexDirection: 'row', paddingBottom: 25, borderBottomWidth: 1,
                    borderBottomColor: 'lightgrey'
                }}>
                    <Text style={{ flex: 1, fontSize: 10, color: '#022A2A' }}>
                        Link your Facebook Account to discover trusted buyers.
                    </Text>
                    <TouchableOpacity style={{
                        flex: 1, borderWidth: 1, alignItems: 'center',
                        justifyContent: 'center', backgroundColor: '#022A2A',
                        marginLeft: 60, marginRight: 20, borderRadius: 10
                    }}>
                        <Text style={{ fontSize: 15, color: 'white', padding: 7 }}>
                            Link
                        </Text>
                    </TouchableOpacity>
                </View>

                <Text style={{
                    marginLeft: 20, fontSize: 16, color: '#022A2A',
                    marginTop: 10
                }}>
                    Google
                </Text>
                <View style={{
                    marginLeft: 20, flexDirection: 'row', paddingBottom: 25, borderBottomWidth: 1,
                    borderBottomColor: 'lightgrey'
                }}>
                    <Text style={{ flex: 1, fontSize: 10, color: '#022A2A' }}>
                        Link your Google Account to seamlesly use your contact list.
                    </Text>
                    <TouchableOpacity style={{
                        flex: 1, borderWidth: 1, alignItems: 'center',
                        justifyContent: 'center', backgroundColor: '#022A2A',
                        marginLeft: 60, marginRight: 20, borderRadius: 10
                    }}>
                        <Text style={{ fontSize: 15, color: 'white', padding: 7 }}>
                            Unlink
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isModalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setIsModalVisible(!isModalVisible);

                }}

                deviceHeight={10}
                deviceWidth={10}
            >
                <View style={StyleEditProfile.modalView}>
                    <TouchableOpacity
                        onPress={GalleryCamera}
                        style={StyleEditProfile.modalTouchable}>
                        <Image style={StyleEditProfile.modalImage} source={Images.Camera_Icon} />
                        <Text style={StyleEditProfile.modalText}>Take a picture</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={GalleryImage}
                        style={StyleEditProfile.modalTouchable}>
                        <Image style={StyleEditProfile.modalImage} source={Images.Gallery_Icon} />
                        <Text style={StyleEditProfile.modalText}>Select from gallery</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={StyleEditProfile.modalTouchable}>
                        <Image style={StyleEditProfile.modalImage} source={Images.F_Icon} />
                        <Text style={StyleEditProfile.modalText}>Import from Facebook</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={StyleEditProfile.modalTouchable}>
                        <Image style={StyleEditProfile.modalImage} source={Images.G_Icon} />
                        <Text style={StyleEditProfile.modalText}>Import from Google</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={StyleEditProfile.modalTouchable}>
                        <Image style={StyleEditProfile.modalImage} source={Images.Delete_Icon} />
                        <Text style={StyleEditProfile.modalText}>Remove picture</Text>
                    </TouchableOpacity>


                </View>
            </Modal>

        </View>


    )
}


