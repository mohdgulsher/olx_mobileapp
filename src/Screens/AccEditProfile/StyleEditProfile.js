import { StyleSheet } from "react-native";

export default StyleSheet.create({
    headerView:{
        flexDirection:'row' ,
            borderBottomWidth:1,
            borderColor:'lightgrey',
            paddingBottom:15,
    },
    headerTouchable1:{
        marginLeft:10,marginRight:15,
            marginTop:15,
    alignSelf:'flex-start'
    },
    headerImageView1:{
        flex:1,
        alignSelf:'flex-start'
    },
    headerImageView2:{
        flex:1,
        alignSelf:'flex-end'
    },
    headerText:{
        right:20,
       fontSize:16,
       color:'#022A2A',
        alignSelf:'flex-end'
    },
    modalView:{
        borderRadius:5,
            height: '45%',
           width:'100%',
           marginTop: 'auto',
           backgroundColor:'lightgrey',
    },
    modalTouchable:{
        flexDirection:'row',marginTop:30,
    },
    modalImage:{
        alignSelf:'center',marginLeft:20,
        height:25,width:25
    },
    modalText:{
        marginLeft:30,fontSize:16,
        color:'#022A2A',alignSelf:'center'
    }
})