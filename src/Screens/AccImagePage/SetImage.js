import React,{useState} from 'react'
import { View, Text,Image,TouchableOpacity } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleSetImage from './StyleSetImage'
import { useNavigation } from '@react-navigation/native'
import { useRoute } from '@react-navigation/native'


export default function SetImage(props) {
    let navigation=useNavigation();
    let route=useRoute();
    const [username,setUsername]=useState(`${props.route.params.getName}`)
    const [usersomething,setUsersomething]=useState('')
    const [userphone,setUserphone]=useState('')
    const [useremail,setUseremail]=useState(`${props.route.params.getEmail}`)
    // const [username,setUsername]=useState('User name')

    
    return (
        <View style={StyleSetImage.container}>
            <View style={StyleSetImage.headerView}>
            <View  style={StyleSetImage.headerImageView1}>
            <TouchableOpacity 
            onPress={()=>{
            route.params.ClickHandle2(username)
            route.params.ClickHandle3(usersomething)
            route.params.ClickHandle4(userphone)
            route.params.ClickHandle5(useremail)
           props.navigation.goBack();
            }
            }
            style={StyleSetImage.headerTouchable1} >
            <Image
             source={Images.Back_Icon}/>
            </TouchableOpacity>
            </View>
            <View style={StyleSetImage.headerImageView2}>
            <TouchableOpacity  style={StyleSetImage.headerTouchable1} >
            <Image 
            style={StyleSetImage.headerImage}
            source={Images.More_Icon}/>
            </TouchableOpacity>
            </View>
            </View>

            <View style={StyleSetImage.stepView}> 
                <TouchableOpacity style={StyleSetImage.stepTouchable}>
                    <Text style={StyleSetImage.stepText1}> You have 2 steps left. </Text>
                    <Text style={StyleSetImage.stepText2}>Complete your profile</Text>
                    <Image style={StyleSetImage.stepImage }
                    source={Images.RightBcak_Icon}/>
                </TouchableOpacity>
                
            </View>
                   
                   <View style={{flexDirection:'row'}} >
                         <View  style={StyleSetImage.accImageView}>
                         <TouchableOpacity 
                         onPress={()=>props.navigation.navigate('ImageShow')
                          
                        }
                         style={StyleSetImage.accImageTouch}>
                         <Image style={StyleSetImage.accImage}
                         source={Images.Acc_Image} />
                         </TouchableOpacity>
                         </View>
                         <View style={{flex:1}}>
                         <View style={StyleSetImage.followView}>
                        
                             <TouchableOpacity style={StyleSetImage.followTouchable}>
                                 <Text  style={StyleSetImage.followText1}>0</Text>
                                 <Text style={StyleSetImage.followText2}>Following</Text>
                             </TouchableOpacity>
                            
                             
                             <TouchableOpacity 
                             style={StyleSetImage.followTouchable1}>
                                 <Text style={StyleSetImage.followText1}>0</Text>
                                 <Text style={StyleSetImage.followText2}>Followers</Text>
                             </TouchableOpacity>
                            
                         </View>
                         <TouchableOpacity 
                           onPress={()=>navigation.navigate('EditProfile',
                           {Clickhandle:setUsername,
                           Clickhandle1:setUsersomething,
                           Clickhandle2:setUserphone,
                           Clickhandle3:setUseremail,
                           getvaluename:props.route.params.getName,
                           getvalueemail:props.route.params.getEmail
                           } )
                           }
                         style={StyleSetImage.editTouchable}>
                                 <Text style={StyleSetImage.editText}>Edit Profile</Text>
                             </TouchableOpacity>
                             </View>
                   </View>
                   <View style={StyleSetImage.userNameView}>
                   <Text style={StyleSetImage.userNameText}>
                   {username}</Text>
                   </View>

        </View>
    )
}
