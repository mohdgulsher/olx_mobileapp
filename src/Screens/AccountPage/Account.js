
import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native'
import AccTouch from '../../Componets/AccComponet/AccTouch'
import { Images } from '../../utils/Constants'
import StyleAccount from './StyleAccount'
import MainButton from '../../Componets/Button/MainButton';
import { useNavigation } from '@react-navigation/native'
let color1 = '';
let color2 = '';
let color3 = '';
let color4 = '';
let color5 = '';
export default function Account(props) {
    const navigation = useNavigation();
    const [userImage, setUserImage] = useState('')
    const [userName, setUserName] = useState('User Name')
    const [userSomething, setUserSomething] = useState('');
    const [userPhone, setUserPhone] = useState('');
    const [userEmail, setUserEmail] = useState('abc@gmail.com')


    if (true) {

        if (userImage == '') {

            color1 = 'lightgrey'
        }
        else {

            color1 = 'yellow';
        }
    }
    if (true) {

        if (userName == '') {

            color2 = 'lightgrey'
        }
        else {

            color2 = 'yellow';
        }
    }
    if (true) {

        if (userSomething == '') {

            color3 = 'lightgrey'
        }
        else {

            color3 = 'yellow';
        }
    }
    if (true) {

        if (userPhone == '') {

            color4 = 'lightgrey'
        }
        else {

            color4 = 'yellow';
        }
    }
    if (true) {

        if (userEmail == '') {

            color5 = 'lightgrey'
        }
        else {
            color5 = 'yellow';
        }
    }

    return (
        <View style={StyleAccount.container}>

            <TouchableOpacity
                onPress={() => navigation.navigate('SetImage',
                    {
                        ClickHandle2: setUserName,
                        ClickHandle3: setUserSomething,
                        ClickHandle4: setUserPhone,
                        ClickHandle5: setUserEmail,
                        getName:userName,
                        getEmail:userEmail,


                    })}
            >
                <View style={StyleAccount.touchView}>
                    <Image style={StyleAccount.touchImage}
                        source={Images.Acc_Image} />
                    <View style={StyleAccount.textView}>
                        <Text style={StyleAccount.text1}>{userName}</Text>
                        <Text style={StyleAccount.text2}>View and edit profile </Text>
                    </View>
                </View>
            </TouchableOpacity>
            <View>
                <TouchableOpacity>
                    <Text style={StyleAccount.stepText}> Steps Count</Text>
                    <View style={StyleAccount.countView}>
                        <View style={[StyleAccount.countInnerView, { backgroundColor: `${color1}` }]}></View>
                        <View style={[StyleAccount.countInnerView1, { backgroundColor: `${color2}` }]}></View>
                        <View style={[StyleAccount.countInnerView1, { backgroundColor: `${color3}` }]}></View>
                        <View style={[StyleAccount.countInnerView1, { backgroundColor: `${color4}` }]}></View>
                        <View style={[StyleAccount.countInnerView2, { backgroundColor: `${color5}` }]}></View>
                        <View style={StyleAccount.countInnerView2}></View>
                    </View>
                    <Text style={{ marginTop: 10, fontSize: 13, marginLeft: 10, marginRight: 10, color: '#022A2A' }}>
                        We are the built on trust Help one another to get know each other better.
                    </Text>

                </TouchableOpacity>
            </View>

            <View style={{ marginTop: 5 }}>
                <AccTouch
                    onPress={() => props.navigation.navigate("InvoicesBilling")}
                    Image1={Images.Buy_Icon}
                    Text1='Buy Packages & My Orders'
                    Text2='Packages,orders,billing and invoices'
                    Image2={Images.RightBcak_Icon}
                />
                <AccTouch
                    onPress={() => props.navigation.navigate("Setting")}
                    Image1={Images.Setting_Icon}
                    Text1='Settings'
                    Text2='Privacy and logout'
                    Image2={Images.RightBcak_Icon}
                />
                <AccTouch
                    onPress={() => props.navigation.navigate("HelpSupport")}
                    Image1={Images.Olx_Icon}
                    Text1='Help & Support'
                    Text2='Help center and legal terms'
                    Image2={Images.RightBcak_Icon}
                />
                <AccTouch
                    Image1={Images.Earth_Icon}
                    Text1='Select Language/Hindi'
                    Text2='English'
                    Image2={Images.RightBcak_Icon}
                />
            </View>

            <MainButton
                title="Login or register"
            />

        </View>
    )
}
