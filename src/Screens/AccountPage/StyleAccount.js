import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1, backgroundColor: 'white'
    },
    touchView: {
        marginTop: 20, flexDirection: 'row', marginLeft: 10
    },
    touchImage: {
        borderRadius: 100, width: 100, height: 100,
        marginTop: 25
    },
    textView: {
        marginTop: 25
    },
    text1: {
        fontSize: 23, marginLeft: 15, color: "#022A2A", fontWeight: 'bold'
    },
    text2: {
        fontSize: 18, marginTop: 1, color: "#022A2A",
        marginLeft: 18, borderBottomWidth: 2, borderBottomColor: "#022A2A"
    },
    stepText:{
        marginTop:20,marginLeft:10,fontSize:16,color:'#022A2A'
    },
    countView:{
        flexDirection:'row', marginTop:10,marginLeft:10,marginRight:10
    },
    countInnerView:{
        flex:1, backgroundColor:'lightgrey',height:7,width:7
    },
    countInnerView1:{
        backgroundColor:'lightgrey',flex:1,marginLeft:10,height:7,width:7
    },
    countInnerView2:{
        backgroundColor:'lightgrey',flex:1,marginLeft:10,height:7,width:7
    },
    betterText:{
        marginTop:10,fontSize:13,
        marginLeft:10,marginRight:10,
        color:'#022A2A'
    }
})