import React, { useState,useEffect } from 'react'
import { View, Text, Image, TouchableOpacity,FlatList } from 'react-native';
import { Images } from '../../utils/Constants';
import axios from 'react-native-axios';
import { useNavigation } from '@react-navigation/native';
import StyleCategory from './StyleCategory';
import AsyncStorage from '@react-native-community/async-storage';

const tokenData = async () => {
    const headers = {
    headers: {
    Authorization: await AsyncStorage.getItem('token'),
        },
      };
    return headers;

    };
   


export default function Category(props) {
    const navigation = useNavigation();
 const [data,setData]=useState([]);
 console.log('start',data)
 

 const getData = async () => {
    let Token=await tokenData();
     console.log('hhhh');
    let res1 = await axios.get("http://192.168.1.20:3000/auth/category",Token);
    console.log('kjhgfds')
    console.log(Token);
    if (res1.status === 200) {
        setData(res1.data.data);
      console.log("success", res1);
      
    } else {
      console.log("catch", res1);
    }

   
  };

  useEffect(()=>{
      getData();
  },[]);
  
  console.log(data);

    const Item = ( {title}) => (
        <View style={StyleCategory.itemView}>
            <TouchableOpacity 
            onPress={() => navigation.navigate('SubCategory',{
                Id:title._id,
                Cata:title.catg_name, })}
                style={{ flexDirection: 'row' }}>
                <Image style={StyleCategory.itemImage}
                    source={{ uri:(title.catg_image) }} />
                <Text style={StyleCategory.itemText}>{title.catg_name}</Text>
                <View style={StyleCategory.itemView1}>
                    <Image style={StyleCategory.itemImage1}
                        source={Images.RightBcak_Icon} /> 
                </View>
            </TouchableOpacity>
        </View>
    
    );

    const renderItem = ({item} ) =>{ 
        console.log(item,"render")
       return <Item title={item}/>
    };
    return (
        <View style={StyleCategory.container}>
            <View style={StyleCategory.headerView}>
                <TouchableOpacity onPress={()=>props.navigation.navigate("Home")}
                    style={StyleCategory.backImageTouch}>
                    <Image
                        source={Images.Back_Icon} />
                </TouchableOpacity>
                <Text style={StyleCategory.text}>Categories</Text>
            </View>

            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
              
            />
          

        </View>
    )
}
