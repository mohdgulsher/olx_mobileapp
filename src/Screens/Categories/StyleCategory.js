import { StyleSheet } from "react-native";

export default StyleSheet.create({
container:{
    flex: 1, 
    backgroundColor: 'white'
},
headerView:{
    flexDirection: 'row',
     borderBottomWidth: 2,
    borderColor: 'lightgrey',
     paddingBottom: 10
},
backImageTouch:{
    marginLeft: 15, 
    marginTop: 12,
     backgroundColor: 'white'
},
text:{
    marginLeft: 24,
     marginTop: 12, fontSize: 20,
    color: '#022A2A'
},
itemView:{
    marginTop: 20
},
itemImage:{
    marginLeft: 20,
     width: 40,
      height: 40 ,
      marginTop:4
},
itemText:{
    marginLeft: 50,
     fontSize: 18,
      marginTop: 10,
    color: "#022A2A"
},
itemView1:{
    flex: 1,
     alignItems: 'flex-end',
      marginTop: 12
},
itemImage1:{
    marginRight: 20

}


})