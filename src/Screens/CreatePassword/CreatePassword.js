import React,{useState} from 'react'
import { View, Text,TouchableOpacity,Image } from 'react-native';
import StyleCreatePassword from './StyleCreatePassword';
import { Images } from '../../utils/Constants';
import FilledText from '../../Componets/FilledText/FilledText';
import MainButton from '../../Componets/Button/MainButton';

export default function CreatePassword(props) {
    const [secureTextEntry, setsecureTextEntry] = useState(true)
    const [secureTextEntry1, setsecureTextEntry1] = useState(true)
     const [newpass,setNewpass]=useState('')
     const [confirmpass,setConfirmpass]=useState('');


    const seen_password = () => {
      setsecureTextEntry(!secureTextEntry)
    }
    const seen_password1 = () => {
        setsecureTextEntry1(!secureTextEntry1)
      }

    return (
        <View style={StyleCreatePassword.container}>
             <View style={StyleCreatePassword.headerView}>
               <TouchableOpacity 
               onPress={()=>props.navigation.navigate("Setting")}
               style={StyleCreatePassword.headerTouchable}>
                   <Image  source={Images.Back_Icon}/>
               </TouchableOpacity>
               <Text style={StyleCreatePassword.headerText}>
                  Create password
               </Text>
           </View>
                  <Text style={{marginTop:20,marginLeft:20,fontSize:13,
                  marginRight:20,color:'#022A2A'}}>
                      Please enter your new password
                  </Text>

                  <FilledText
            label="New password"
            keyboardType="text"
            imageText={Images.Eyes_Icon}
            onPress={seen_password}
            secureTextEntry={secureTextEntry}
            maxLength={12}
            lineWidth={0}
            activeLineWidth={0}
            tintColor="aqua"
            value={newpass}
            onChangeText={(newpass)=>setNewpass(newpass)}
            // style1={{ borderBottomWidth: 0 }}
          />
                <FilledText
            label="Confirm new Password"
            keyboardType="text"
            imageText={Images.Eyes_Icon}
            onPress={seen_password1}
            secureTextEntry={secureTextEntry1}
            maxLength={12}
            lineWidth={0}
            activeLineWidth={0}
            tintColor="aqua"
            value={confirmpass}
            onChangeText={(confirmpass)=>setConfirmpass(confirmpass)}
            // style1={{ borderBottomWidth: 0 }}
          />

          <View style={{flex:1,marginTop:50}}>
          <MainButton
          // onPress={}
        
            title="Set Password"
          />
          </View>

        </View>
    )
}
