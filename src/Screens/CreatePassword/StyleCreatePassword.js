import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container:{
        flex:1,backgroundColor:'white'
    },
    headerView:{
        flexDirection:'row',paddingBottom:15,
        borderBottomColor:'lightgrey',
           borderBottomWidth:1
    },
    headerTouchable:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:20,marginRight:10,marginTop:10
    },
    headerText:{
        alignSelf:'center',marginLeft:20,
        marginRight:50,
               marginTop:10,fontSize:18,
               color:'#022A2A',fontWeight:'bold'
    },
})