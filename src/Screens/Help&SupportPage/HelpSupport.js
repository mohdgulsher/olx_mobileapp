import { PROPERTY_TYPES } from '@babel/types'
import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import InnerText from '../../Componets/AccInnerText/InnerText'
import { Images } from '../../utils/Constants'
import StyleHS from './StyleHS'

export default function HelpSupport(props) {
    return (
        <View style={StyleHS.container}>
            <View style={StyleHS.headerView}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Account")}
                    style={StyleHS.headerTouchable}>
                    <Image source={Images.Back_Icon} />
                </TouchableOpacity>
                <Text style={StyleHS.headerText}>
                    Help & Support
                </Text>
            </View>


            <InnerText
                Text1="Help Center"
                Text2="See FAQ and contact support"
                Image={Images.RightBcak_Icon}
            />
            <InnerText
                Text1="Rate us"
                Text2="If you love our app,please take a moment to rate it"
                Image={Images.RightBcak_Icon}
            />
            <InnerText
                Text1="Invite friends to OLX"
                Text2="Invite your friends to buy and sell"
                Image={Images.RightBcak_Icon}
            />
            <InnerText
                Text1="Become a beta tester"
                Text2="Test new features in advance"
                Image={Images.RightBcak_Icon}
            />
             <InnerText
                Text1="Version"
                Text2="14.40.001"
               
            />


        </View>
    )
}
