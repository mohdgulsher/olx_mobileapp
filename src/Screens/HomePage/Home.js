import React,{useState,useEffect} from 'react'
import { View, Text, TextInput, TouchableOpacity, Image, Button, ScrollView, FlatList } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleHome from './StyleHome';
import { Data } from '../../datajson';
import Category from '../Categories/Category';
import axios from 'react-native-axios';
import AsyncStorage from '@react-native-community/async-storage';

const tokenData = async () => {
    const headers = {
    headers: {
    Authorization: await AsyncStorage.getItem('token'),
        },
      };
    return headers;
  
    };

const Item = ({ title }) =>{
    console.log('title',title.image[0]);
     return(
    <View style={StyleHome.item}>
     
    <TouchableOpacity>
    
    <Image style={StyleHome.itemImage}
         source={{uri:`http://192.168.1.20:3000/${title.image[0]}`}}
        //  source={require(title.image[0])}

         />
         <View style={StyleHome.itemHeartView}>
             <TouchableOpacity>
                 <Image style={StyleHome.heartImage}
                 source={Images.Heart_Icon}/>
             </TouchableOpacity>
         </View>
         <Text style={StyleHome.title}>{title.price}</Text>
        <Text style={StyleHome.titleText}>{title.title}</Text>
        <Text style={StyleHome.titleText}> {title.description}</Text>
        <Text style={StyleHome.titleText}> {title.state}</Text>
        <Text style={StyleHome.titleText}> {title.city}</Text>
    </TouchableOpacity>
    </View>
 
)
};


export default function Home(props) {

    const [data,setData]=useState([]);

    const getData = async () => {
        let Token=await tokenData();
          let result = await axios.get("http://192.168.1.20:3000/auth/listItem",Token);
          if (result.status === 200) {
            console.log("success", result);
            setData(result.data.data, "ListData");
          } else {
            console.log("catch", result);
          }
        };
  
        useEffect(()=>{
            getData()
        },[])
        console.log(data);


    const renderItem = ({ item }) => {
        // console.log("render",item)
    
        return (
            <Item title={item}/>

        );
    };



    return (
        <View style={StyleHome.Container}>

            <View style={StyleHome.View}>
                <TouchableOpacity  onPress={()=>{props.navigation.navigate('Search')}}
                style={StyleHome.touchable} >
                    <Text style={StyleHome.T_text}
                    >Find Cars,Mobile Phone and more....</Text>
                    <Image style={StyleHome.textimage}
                        source={Images.Search_Icon} />
                </TouchableOpacity>
                <TouchableOpacity style={StyleHome.image}>
                    <Image source={Images.Notification_Icon} />
                </TouchableOpacity>
            </View>
            <ScrollView>
                <View style={StyleHome.s_view}>

                    <Text style={StyleHome.text1}>
                        Olx AUTOS
                    </Text>
                    <Text style={StyleHome.text2} >New Way to </Text>
                    <Text style={StyleHome.text3}>Buy & Sell Car</Text>
                    <Image style={StyleHome.carimage}
                        source={Images.Car_Image}
                    />
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={StyleHome.button1}>
                            <Text style={StyleHome.buttonText}>Buy Car</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={StyleHome.button2}>
                            <Text style={StyleHome.buttonText}>Sell Car</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={StyleHome.browseView}>
                    <Text style={StyleHome.browseText}>Browse Catagories</Text>
                    <View style={StyleHome.SeeView}>
                        <TouchableOpacity 
                        onPress={()=>props.navigation.navigate(Category)} >
                            <Text style={StyleHome.seeText}>See all</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                <ScrollView horizontal={true}
                    style={StyleHome.SVHscrollview} >
                    <TouchableOpacity style={StyleHome.carTouchable}>
                        <Image
                            style={StyleHome.SVHimage}
                            source={Images.Phone1_Icon} />
                        <Text style={StyleHome.SVHtext}>MOBILE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image style={StyleHome.SVHimage}
                            source={Images.Fashion_Icon} />
                        <Text style={StyleHome.SVHtext}>Fashion</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image
                            style={StyleHome.SVHimage}
                            source={Images.Furniture_Icon} />
                        <Text style={StyleHome.SVHtext}>Furniture</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image style={StyleHome.SVHimage}
                            source={Images.Pets_Icon} />
                        <Text style={StyleHome.SVHtext}>Pets</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image
                            style={StyleHome.SVHimage}
                            source={Images.Car1_Icon} />
                        <Text style={StyleHome.SVHtext}>OLX AUTOS(CAR)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image style={StyleHome.SVHimage}
                            source={Images.Bike_Icon} />
                        <Text style={StyleHome.SVHtext}>BIKE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image
                            style={StyleHome.SVHimage}
                            source={Images.Phone1_Icon} />
                        <Text style={StyleHome.SVHtext}>MOBILE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={StyleHome.SVHtouchable}>
                        <Image style={StyleHome.SVHimage}
                            source={Images.Laptop_Icon} />
                        <Text style={StyleHome.SVHtext}>LAPTOP</Text>
                    </TouchableOpacity>
                </ScrollView>
                <View style={StyleHome.freshView}>
                    <Text style={StyleHome.freshText}>Fresh Recommendations</Text>

                </View>
               

                <View style={StyleHome.container}>
                        <FlatList
                            data={data}
                            renderItem={renderItem}
                            numColumns={2}
                        />
                    </View>
                    </ScrollView>
           
        </View>
    )
}


