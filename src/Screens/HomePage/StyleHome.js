import { StyleSheet } from "react-native";

export default StyleSheet.create({
   Container:{
    flex: 1,
     backgroundColor: 'white' 
   },
   View:{
    paddingBottom: 5, 
    marginTop: 10, 
    borderBottomWidth: 1,
    borderBottomColor: 'lightgrey',
     flexDirection: 'row',
   },
   touchable:{
    flex: 1,
     borderRadius: 10,
    borderWidth: 1,
     padding: 6,
    marginLeft: 10,
     marginRight: 10,alignSelf:'center'
   },
   T_text:{
    color: '#022A2A',
     fontSize: 15,
      marginLeft: 20,
      alignSelf:'center'
   },
   textimage:{
    position: 'absolute',
     marginTop: 9,
      marginLeft: 10,
     
   },
   image:{
    marginLeft: 8,
    marginRight: 20, 
    marginTop: 10
   },
   s_view:{
    backgroundColor: 'blue',
     marginTop: 10,
    marginLeft: 10,
     marginRight: 10, 
     paddingBottom: 20, 
     borderRadius: 10
   },
   text1: {
    fontSize: 25, 
    color: 'white',
     marginTop: 15,
    marginLeft: 20
   },
   text2:{
    fontSize: 18, 
    color: 'white',
    marginTop: 30,
     marginLeft: 15
   },
   text3:{
    fontSize: 18, 
    color: 'white',
    marginTop: 10,
     marginLeft: 15
   },
   carimage:{
    position: 'absolute',
     alignSelf: 'flex-end',
    marginTop: 30 
   },
   button1:{
    flex: 1,
     backgroundColor: 'white',
      marginLeft: 30,
    marginTop: 30, 
    marginRight: 20, 
    padding: 15,
     borderRadius: 10
   },
   buttonText:{
    alignSelf: 'center',
     fontSize: 16,
      color: '#022A2A'
   },
   button2:{
    flex: 1,
     backgroundColor: 'white',
    marginTop: 30,
     marginRight: 30,
      padding: 15,
       borderRadius: 10
   },
   browseView:{
    flexDirection: 'row',
     flex: 1,
     marginLeft: 10,
      marginTop: 25,
   },browseText:{
    fontSize: 16,
     color: '#022A2A' 
   },
   SeeView:{
    flex: 1,
     alignItems: 'flex-end',
      paddingRight: 20
   },
   seeText:{
    color: '#022A2A', 
    fontSize: 14, 
    borderBottomWidth: 1
   },
   carTouchable:{
    alignItems:'center',
    marginLeft:10 
   },
   SVHimage:{
    height:50,width:50 
   },
   SVHtext:{
    fontSize:12,
    marginTop:4,
    color:'#022A2A'
   },
   SVHtouchable:{
    alignItems:'center',
    marginLeft:40
   },
   SVHscrollview:{
    marginTop: 10 ,
    paddingBottom:10,
    borderBottomWidth:7,
    borderBottomColor:'lightgrey'
   },
  
  freshView:{
    marginLeft: 10, 
    marginTop: 10   
  },
  freshText:{
    marginTop: 15,
     fontSize: 16, 
     color: '#022A2A'
  },
  container: {
    flex: 1,
    marginTop:10
  },
  item: {
      flex:1,
    marginVertical: 3,
    marginHorizontal:3,
    borderRadius:5,
    borderWidth:2,
    borderColor:'lightgrey'
  },
  itemImage:{
    flex:1,
    width:160,
    height:130,
    alignSelf:'center',
    marginTop:10
  },
  itemHeartView:{
    position:'absolute',right:20,top:20,
    borderRadius:20,backgroundColor:'white',
    height:30,width:30,
    alignItems:'center',justifyContent:'center',
    borderWidth:0.3,borderColor:'lightgrey'
  },
  heartImage:{
    width:25,height:25,
  },
  title: {
      flex:1,
      fontSize:16,
      marginLeft:15,
      marginTop:10,
      color:'#022A2A'
  },
  titleText:{
    fontSize:12,
    marginLeft:16,
    marginRight:30,
    marginTop:4,
    color:'#022A2A'
  }
})