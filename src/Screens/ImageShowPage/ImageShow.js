import { PROPERTY_TYPES } from '@babel/types'
import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleImageShow from './StyleImageShow'

export default function ImageShow(props) {
    return (
        <View style={StyleImageShow.container}>
        <TouchableOpacity onPress={()=>props.navigation.navigate('SetImage')}
        style={StyleImageShow.touchable}>
            <Image source={Images.Back_Icon} />
        </TouchableOpacity>
         <View style={StyleImageShow.ImageView}>
        <Image style={StyleImageShow.image}
        source={Images.Acc_Image}/>
        </View>
            
        </View>
    )
}
