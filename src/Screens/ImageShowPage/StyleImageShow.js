import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container:{
        flex:1,backgroundColor:'black'
    },
    touchable:{
        marginTop:25,marginLeft:20,
        alignSelf:'flex-start'
    },
    ImageView:{
        flex:1,alignItems:'center',
        justifyContent:'center'
    },
    image:{
        width:350,height:350
    }
})