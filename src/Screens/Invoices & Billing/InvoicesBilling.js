import { PROPERTY_TYPES } from '@babel/types'
import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import InnerText from '../../Componets/AccInnerText/InnerText'
import { Images } from '../../utils/Constants'
import StyleIB from './StyleIB'

export default function InvoicesBilling(props) {
    return (
        <View style={StyleIB.container}>
            <View style={StyleIB.headerView}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Account")}
                    style={StyleIB.headerTouchable}>
                    <Image source={Images.Back_Icon} />
                </TouchableOpacity>
                <Text style={StyleIB.headerText}>
                    Invoices & Billing
                </Text>
            </View>


            <InnerText
                Text1="Buy packages"
                Text2="Sell faster,more & at higher margins with packages"
                Image={Images.RightBcak_Icon}
            />
            <InnerText
                Text1="My Orders"
                Text2="Active.Scheduled and expired orders"
                Image={Images.RightBcak_Icon}
            />
            <InnerText
                Text1="Invoices"
                Text2="See and download your invoices"
                Image={Images.RightBcak_Icon}
            /><InnerText
                Text1="Billing information"
                Text2="Sell faster,more & at higher margins with packages"
                Image={Images.RightBcak_Icon}
            />


        </View>
    )
}
