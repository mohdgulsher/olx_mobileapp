import React, { useState, createRef } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, Alert } from 'react-native'
import { Images } from '../../utils/Constants';
import FilledText from '../../Componets/FilledText/FilledText';
import MainButton from '../../Componets/Button/MainButton';
import StyleSign from './StyleSign';
import axios from 'react-native-axios';
import AsyncStorage from '@react-native-community/async-storage';

export default function LoginSign(props) {
  const fieldRef = createRef()


  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  console.log(password);
  console.log(email);
  const [secureTextEntry, setsecureTextEntry] = useState(true)

  const seen_password = () => {
    setsecureTextEntry(!secureTextEntry)
  }

  

  const Validation=()=>{
    if (email == '') {
      Alert.alert('Fill the Email ')
    }
    else if (password == '') {
      Alert.alert('Fill the Password');
    }
  }

  const onSubmt =async() => {
   const res= await AsyncStorage.getItem('token');
   console.log(res);
        if(Validation()){
          console.log('done');
        }
    else{

      const data={email:email,password:password}
      console.log(data);
     
      try{
      const result = await axios.post("http://192.168.1.20:3000/auth/login",data)
      if(result.status===200) {
     console.log(result,'Success');
     props.navigation.navigate('Home');
    // console.log(result.data.token , "token");
   
     try{
      await AsyncStorage.setItem("token",result.data.token)
     }catch(error){
       console.log(error,' token problem')
     }

    }
  }catch(err){
    alert(err.response.data.message);
    console.log('data error');
  }
    }

  }

  return (
    <View style={StyleSign.containerView}>

      <View style={StyleSign.View}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('Login')}
          style={StyleSign.touchable}>
          <Image source={Images.Back_Icon} />
        </TouchableOpacity>
        <Text style={StyleSign.text}>Login</Text>
      </View>
      <ScrollView style={StyleSign.sView}>
        <Image style={StyleSign.image}
          source={Images.Baz_Image}

        />
        <Text style={StyleSign.welcomeText}>

          Welcome</Text>
        <Text style={StyleSign.loginText}>Login to Continue</Text>

        <View style={StyleSign.filledView} >
          <FilledText
            label="Enter Your Email"
            keyboardType="text"
            value={email}
            onChangeText={(email) => setEmail(email)}
            lineWidth={0}
            activeLineWidth={0}
            tintColor="#022A2A"

          />
          <FilledText
            value={password}
            onChangeText={(password) => setPassword(password)}
            label="Password"
            keyboardType="text"
            imageText={Images.Eyes_Icon}
            onPress={seen_password}
            secureTextEntry={secureTextEntry}
            maxLength={12}
            lineWidth={0}
            activeLineWidth={0}
            tintColor="#022A2A"
          />

        </View>
        <View style={StyleSign.buttonView}>
          <MainButton
          onPress={onSubmt}
          // onPress={()=> props.navigation.navigate('Home')}
            title="Login"
          />
        </View>

      </ScrollView>

    </View>
  )
}
