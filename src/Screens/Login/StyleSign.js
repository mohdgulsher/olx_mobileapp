import { StyleSheet } from "react-native";

export default StyleSheet.create({
    containerView:{
        backgroundColor: 'white',
         flex: 1 
    },
    View:{
        flexDirection: 'row',
         marginTop: 10,
        alignItems: 'center',
         borderBottomWidth: 1,
          paddingBottom: 10
    },
    touchable:{
        marginLeft: 20 
    },
    text:{
        fontSize: 16, 
        fontWeight: 'bold',
        color: 'black', 
        marginLeft: 25
    },
    sView:{
        flex:1,
    },
    image:{
        marginTop: 20, 
        marginLeft: 20,
        borderRadius: 40,
        height:50,
        width:50
    },
    welcomeText:{
        fontSize: 30, 
        fontWeight: 'bold', 
        color: 'black',
        marginLeft: 25,
         marginTop: 20
    },
    loginText:{
        marginLeft: 25, 
        fontSize: 19,
         marginRight: 50,
        color: 'black',
         marginTop: 5
    },
    filledView:{
        flex: 0,
         marginTop: 40
    },
    buttonView:{ 
        marginTop: 50,
        marginBottom:70
    }
})