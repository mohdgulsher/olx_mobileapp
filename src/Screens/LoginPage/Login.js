import React from 'react'
import { View, Text, Image, TouchableOpacity, } from 'react-native'
import { Images } from '../../utils/Constants'
import TextComponent from '../../Componets/TextInput/TextComponent'
import StyleLogin from './StyleLogin'
import { useLinkProps } from '@react-navigation/native'

export default function Login(props) {
    return (
        <View style={StyleLogin.mainView}>
            <View style={StyleLogin.touchableView}>
                <TouchableOpacity
                onPress={()=>props.navigation.navigate('Home')}
                    style={StyleLogin.touchable}>
                    <Image source={Images.Cross_Icon}></Image>
                </TouchableOpacity>
                <Image style={StyleLogin.image}
                    source={Images.Logo_Image}></Image>
                <Text style={StyleLogin.text}>INDIA</Text>
            </View>
            <View style={StyleLogin.halfView}> 
                <View style={StyleLogin.Button}>
                <TextComponent
                onPress={()=>props.navigation.navigate('LoginSign')}
                    image={Images.Phone_Icon}
                        text="Continue with Login"
                    />
                <TextComponent
                    image={Images.Google_Icon}
                    text="Continue with Google"
                />
                <TextComponent
                    image={Images.F_Icon}
                    text="Continue with Facebook"
                />
                </View>
                <View style={{flex:0}}>
                <Text style={StyleLogin.OrText}>OR</Text>
                <TouchableOpacity
                onPress={()=>props.navigation.navigate('Signup')}
                 style={StyleLogin.LoginTouchable}>
                    <Text style={StyleLogin.LoginText}>SignUp</Text>
                </TouchableOpacity>
                <Text  style={StyleLogin.continueText}>If you continue,you are accepting</Text>
              <TouchableOpacity style={StyleLogin.PrivacyTouchable}>
                <Text  style={StyleLogin.privacyText}>OLX Terms and Conditions and Privacy Policy</Text>
               </TouchableOpacity>
                </View>
      </View>
     

        </View>
    )
}
