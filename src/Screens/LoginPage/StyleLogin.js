import { StyleSheet } from "react-native";

export default StyleSheet.create({
    mainView: {
        flex: 1
    },
    touchableView: {
        flex: 1,
        backgroundColor: 'white',
    },
    touchable: {
        marginTop: 30,
        marginLeft: 23,
        marginRight: 23
    },
    image: {
        marginTop: 60,
        alignSelf: 'center'
    },
    text: {
        alignSelf: 'center',
        marginTop: 25,
        fontSize: 25,
        fontWeight: 'bold',
        color: '#022A2A'

    },
    halfView: {
        flex: 0,
        backgroundColor: '#022A2A',
        paddingBottom: 30
    },
    Button: {
        marginTop: 10
    },
    OrText: {
        color: 'white',
        alignSelf: 'center'
        , fontSize: 12,
        marginTop: 5
    },
    LoginTouchable: {
        alignSelf: 'center',
        fontSize: 12,
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'white'
    },
    LoginText:{
        color:'white'
    },
    continueText:{
        color:'white',
        alignSelf:'center',
                fontSize:10,
                marginTop:30
    },
    PrivacyTouchable:{
        marginTop:5,
        borderBottomWidth:1,
        borderBottomColor:'white',
        alignSelf:'center',
    },
    privacyText:{
        color:'white',
        fontSize:12,
               
    }

})