import React, { useState } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Images } from '../../utils/Constants'
import StyleSearch from './StyleSearch'

export default function Search(props) {

    const [searchValue, setSearchValue] = useState('')
    const [shouldShow, setShouldShow] = useState(false);

    const onSearch = () => {
        if (searchValue == '') {
            console.log('Hello')
            setShouldShow(false)
        }
        else {
            console.log('Bye')
            setShouldShow(true)
        }
    }
    const  onSubmit = () => {
        setSearchValue('')
        setShouldShow(false)
    }
    return (
        <View style={StyleSearch.container}>
            <View style={StyleSearch.mainView}>
                <View style={ StyleSearch.FindView }>
                    <TouchableOpacity style={StyleSearch.backimage} >
                        <Image
                            source={Images.Back_Icon} />
                    </TouchableOpacity>
                    <TextInput style={StyleSearch.inputText}
                        placeholder="Find Cars,Mobile phone and More"
                        onChangeText={(val) => setSearchValue(val)}
                        value={searchValue}
                        onTextInput={onSearch}
                    />
                    <TouchableOpacity onPress={onSubmit}
                        style={StyleSearch.crossTouch}>
                        {shouldShow ? (<Image style={StyleSearch.crossimage}
                            source={Images.Cross_Icon} />) : null}
                    </TouchableOpacity>
                </View>
                <View style={StyleSearch.searchView}>
                    <TouchableOpacity>
                        <Image 
                        source={Images.Search1_Icon} />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={StyleSearch.popularView}>
                <Text style={StyleSearch.popular_Text}>POPULAR CATEGORIES</Text>
                <TouchableOpacity style={StyleSearch.catergory_Button}>
                    <Image style={StyleSearch.search_Logo} source={Images.Search_Icon} />
                    <Text style={StyleSearch.category_Text}>Fashion</Text>
                </TouchableOpacity>
                <TouchableOpacity style={StyleSearch.catergory_Button}>
                    <Image style={StyleSearch.search_Logo} source={Images.Search_Icon} />
                    <Text style={StyleSearch.category_Text}>Mobiles</Text>
                </TouchableOpacity>
                <TouchableOpacity style={StyleSearch.catergory_Button}>
                    <Image style={StyleSearch.search_Logo} source={Images.Search_Icon} />
                    <Text style={StyleSearch.category_Text}>Furniture</Text>
                </TouchableOpacity>
            </View>


        </View>


    )
}
