import { StyleSheet } from 'react-native'
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        flexDirection: 'row',
        height: 70,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: 'lightgrey',
        padding: 13,
    },
       FindView: {
        flex: 1,
        flexDirection: 'row', marginTop: 1,
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'aqua',
        alignItems: 'center',
    },
    backimage: {
        marginLeft: 10
    },
    inputText: {
        flex: 0, marginLeft: 8
    },
    crossTouch: {
        marginLeft: 30,
        position: 'absolute',
        right: 4
    },
    crossimage: {
        height: 18,
        width: 18
    },
    searchView: {
        width: 40,
        backgroundColor: '#022A2A',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    popular_Text: {
        fontSize: 12,
        color: '#022A2A',
        // fontWeight:'400',
        marginBottom: 20
    },
    popularView: {
        padding: 20
    },
    search_Logo: {
        height: 25,
        width: 25
    },
    catergory_Button: {
        flexDirection: 'row',
        marginTop: 6,
        marginBottom: 6
    },
    category_Text: {
        color: '#022A2A',
        fontSize: 18,
        marginLeft: 15
    },


})