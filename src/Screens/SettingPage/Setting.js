import { PROPERTY_TYPES } from '@babel/types'
import React from 'react'
import { View, Text, TouchableOpacity,Image } from 'react-native'
import InnerText from '../../Componets/AccInnerText/InnerText'
import { Images } from '../../utils/Constants'
import StyleSetting from './StyleSetting'

export default function Setting(props) {
    return (
        <View style={StyleSetting.container}>
           <View style={StyleSetting.headerView}>
               <TouchableOpacity 
               onPress={()=>props.navigation.navigate("Account")}
               style={StyleSetting.headerTouchable}>
                   <Image  source={Images.Back_Icon}/>
               </TouchableOpacity>
               <Text style={StyleSetting.headerText}>
                  Settings
               </Text>
           </View>

          
              <InnerText
              onPress={()=>props.navigation.navigate('CreatePassword')}
                  Text1="Privacy"
                  Text2="Create password"
                  Image={Images.RightBcak_Icon}
              />
              <InnerText
                  Text1="Notifications"
                  Text2="Recommendations & Special communications"
                 
              />
              <TouchableOpacity style={StyleSetting.logoutTouchable}>
             <Text style={StyleSetting.logoutText}>
                      Logout
             </Text>
             </TouchableOpacity>
             <TouchableOpacity style={StyleSetting.logoutTouchable}>
             <Text style={StyleSetting.logoutText}>
                      Logout from all devices
             </Text>
             </TouchableOpacity>
             <TouchableOpacity style={StyleSetting.logoutTouchable}>
             <Text style={StyleSetting.logoutText}>
                      Delete account
             </Text>
             </TouchableOpacity>
         

        </View>
    )
}
