import React, { useState } from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity, Alert } from 'react-native'
import MainButton from '../../Componets/Button/MainButton'
import FilledText from '../../Componets/FilledText/FilledText'
import { Images } from '../../utils/Constants'
import axios from 'react-native-axios';
import StyleSignup from './StyleSignup'

export default function Signup(props) {
    const [secureTextEntry, setsecureTextEntry] = useState(true)

    const seen_password = () => {
        setsecureTextEntry(!secureTextEntry)
    }

    const [secureTextEntry1, setsecureTextEntry1] = useState(true)

    const seen_password1 = () => {
        setsecureTextEntry1(!secureTextEntry1)
    }

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [confirmpass, setConfirmpass] = useState('');

    const onValidation = () => {
        if (name == '') {
            Alert.alert('Fill the Name')
        }
        else if (email == "") {
            Alert.alert('Fill the Email')
        }
        else if (phone == '') {
            Alert.alert('Fill the Phone Number')
        }
        else if (password == '') {
            Alert.alert('Fill the Password');
        }
        else if (confirmpass == '') {
            Alert.alert('Fill the Confirm Password');
        }
        else if (password !== confirmpass) {
            Alert.alert('Oops password does not match');
        }

    }

    const onSubmit = async () => {
        if (onValidation()) {
            console.log('Done');
        }

        else {
            const data = {
                name: name, email: email, phone: phone,
                password: password
            }
            console.log(data);

            try {
                let result = await axios.post("http://192.168.1.20:3000/auth/register", data)
                console.log(result, "-------------------")
                if (result.status === 200) {
                    console.log("Success", result);
                    props.navigation.navigate('LoginSign');
                }
                // else{
                //     console.log('error',result);
                // }
            } catch (err) {
                alert(err.response.data.message);
            }
        }

    }

    return (
        <View style={StyleSignup.container}>
            <ScrollView>

            <View style={StyleSignup.View}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('Login')}
          style={StyleSignup.touchable}>
          <Image source={Images.Back_Icon} />
        </TouchableOpacity>
        <Text style={StyleSignup.text}>Signup</Text>
      </View>

                <View style={StyleSignup.imageView}>
                    <Image
                        source={Images.Sign_Image} />
             <Text style={StyleSignup.text1}>Signup</Text>
                    <Text style={StyleSignup.text2}>Welcome & Sign up to Continue</Text>
                </View>

                <View style={StyleSignup.filledView}>
                    <FilledText
                        label="Enter your name"
                        keyboardType="text"
                        lineWidth={0}
                        activeLineWidth={0}
                        tintColor="#022A2A"
                        value={name}
                        onChangeText={(name) => setName(name)}

                    />
                    <FilledText
                        label="Enter your Email"
                        keyboardType="text"
                        lineWidth={0}
                        activeLineWidth={0}
                        tintColor="#022A2A"
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                    <FilledText
                        label="Enter your Phone Number"
                        keyboardType="phone-pad"
                        lineWidth={0}
                        activeLineWidth={0}
                        tintColor="#022A2A"
                        value={phone}
                        maxLength={10}
                        onChangeText={(phone) => setPhone(phone)}
                    />
                    <FilledText
                        label="Password"
                        keyboardType="text"
                        imageText={Images.Eyes_Icon}
                        onPress={seen_password}
                        secureTextEntry={secureTextEntry}
                        maxLength={12}
                        lineWidth={0}
                        activeLineWidth={0}
                        tintColor="#022A2A"
                        value={password}
                        onChangeText={(password) => setPassword(password)}
                    />
                    <FilledText
                        label="Confirm Password"
                        keyboardType="text"
                        imageText={Images.Eyes_Icon}
                        onPress={seen_password1}
                        secureTextEntry={secureTextEntry1}
                        maxLength={12}
                        lineWidth={0}
                        activeLineWidth={0}
                        tintColor="#022A2A"
                        value={confirmpass}
                        onChangeText={(confirnpass) => setConfirmpass(confirnpass)}
                    />
                </View>
                <View style={StyleSignup.buttomView}>
                    <MainButton
                        onPress={onSubmit}
                        title="SignUp"
                    />
                </View>
                <View style={StyleSignup.accountView}>
               
                    <Text style={StyleSignup.accountText}>Already have an account? </Text>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('LoginSign')}>
                        <Text style={StyleSignup.loginText}>Login</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
