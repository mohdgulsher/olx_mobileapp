import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    View:{
        flexDirection: 'row',
         marginTop: 10,
        alignItems: 'center',
         borderBottomWidth: 1,
          paddingBottom: 10
    },
    touchable:{
        marginLeft: 20 
    },
    text:{
        fontSize: 18, 
        fontWeight: 'bold',
        color: 'black', 
        marginLeft:30
    },
    imageView: {
        marginTop: 5,
        marginLeft: 20
    },
    text1: {
        marginTop: 5,
        fontSize: 25,
        color: '#022A2A'
    },
    text2: {
        marginTop: 1,
        fontSize: 17,
        color: '#022A2A'
    },
    filledView: {
        flex: 0,
        // marginTop: 10
    },
    buttomView: {
        flex: 1,
        marginTop: 10,
    },
    accountView: {
        flexDirection: 'row',
        marginBottom: 70,
        alignSelf: 'center',
        marginTop: 20
    },
    accountText:{
        fontSize: 15,
         color: '#022A2A'
    },
    loginText:{
        fontSize: 16,
         color: '#022A2A'
    }
})