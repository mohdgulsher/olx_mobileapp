import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    headerView:{
        flexDirection:'row',
        borderBottomWidth:2,
        borderColor:'lightgrey',
        paddingBottom:10
    },
    backImage:{
        marginLeft:15,
        marginTop:12,
        backgroundColor:'white'
    },
    headerText:{
        marginLeft:24,
        marginTop:12,
        fontSize:20,
            color:'#022A2A'
    },
    itemTouchable:{
        marginTop:15,
    },
    itemText:{
        color:'#022A2A',
        fontSize:18,
        marginLeft:40,
    }
})