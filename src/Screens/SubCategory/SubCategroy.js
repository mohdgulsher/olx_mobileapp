import React, { useState,useEffect } from 'react'
import { View, Text,Image,TouchableOpacity,FlatList } from 'react-native';
import { Images } from '../../utils/Constants';
import axios from 'react-native-axios';
import { useRoute } from '@react-navigation/native';
import StyleSubCate from './StyleSubCate';
import AsyncStorage from '@react-native-community/async-storage';


const tokenData = async () => {
  const headers = {
  headers: {
  Authorization: await AsyncStorage.getItem('token'),
      },
    };
  return headers;

  };


export default function SubCategroy(props) {
    const route = useRoute();
    const [subdata,setSubdata]=useState([]);
    const getData = async () => {
      let Token=await tokenData();
        let res1 = await axios.get(`http://192.168.1.20:3000/auth/subCategory/${route.params.Id}`,Token);
        if (res1.status === 200) {
          console.log("success", res1);
          setSubdata(res1.data.data, "SubCategories");
        } else {
          console.log("catch", res1);
        }
      };

      useEffect(()=>{
          getData()
      },[])
      console.log(subdata);

    const Item = ({ title }) => (
        <View>
        <TouchableOpacity style={StyleSubCate.itemTouchable}>
            <Text style={StyleSubCate.itemText}>{title.sub_name}</Text>
        </TouchableOpacity>
    </View>
      );

    const renderItem = ({ item }) => (
        <Item title={item} />
      );
    return (
        <View style={StyleSubCate.container}>

      
             <View style={StyleSubCate.headerView}>
        <TouchableOpacity onPress={()=>props.navigation.navigate('Category')}
        style={StyleSubCate.backImage}>
            <Image
            source={Images.Back_Icon}/>
            </TouchableOpacity>
            <Text style={StyleSubCate.headerText}>{route.params.Cata}</Text>
        </View>

        <FlatList
        data={subdata}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
        </View>
    )
}
