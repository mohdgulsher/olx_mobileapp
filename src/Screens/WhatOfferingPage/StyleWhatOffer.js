import { StyleSheet } from "react-native";
export default StyleSheet.create({
    Container: {
        flex: 1, backgroundColor: 'white'
    },
    headerView: {
        flexDirection: 'row', paddingBottom:15,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
       
    },
    headerTouchable: {
        alignItems: 'center',
        justifyContent: 'center',
       
        marginLeft: 20,
         marginRight: 10,
          marginTop: 10
    },
    headerText: {
        alignSelf: 'center', marginLeft: 20,
        marginRight: 50,
        marginTop: 10, fontSize: 18,
        color: '#022A2A', fontWeight: 'bold'
    },
    container: {
        flex: 1,
        marginTop:10
      },
      item: {
        flex:1,
    //   marginVertical: 0,
    //   marginHorizontal:0,
    //   borderRadius:5,
      borderBottomWidth:1,
      borderRightWidth:1,
      borderColor:'lightgrey',
    // alignItems:'center',
    // justifyContent:'center'
    },
      itemImage:{
        flex:1,
        width:50,
        height:50,
        alignSelf:'center',
        marginTop:10
      },
    
      title: {
          flex:1,
          fontSize:16,
          marginLeft:15,
          marginTop:10,
          color:'#022A2A',
          alignSelf:'center',
      },
     
})