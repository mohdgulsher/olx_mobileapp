import React from 'react'
import { View, Text ,TouchableOpacity,Image,FlatList} from 'react-native'
import StyleWhatOffer from './StyleWhatOffer'
import { Images } from '../../utils/Constants'
import { Data } from '../../datajson';

const Item = ({ item }) => (
    <View style={StyleWhatOffer.item}>
    <TouchableOpacity>
    <Image style={StyleWhatOffer.itemImage}
         source={item.image}/>
         <Text style={StyleWhatOffer.title}>{item.name}</Text>
      
    </TouchableOpacity>
    </View>
);

export default function WhatOffer(props) {

    const renderItem = ({ item }) => {

        return (
            <Item
                item={item.title}
               
            />
        );
    };

    return (
        <View style={StyleWhatOffer.Container}>
             <View style={StyleWhatOffer.headerView}>
               <TouchableOpacity 
               onPress={()=>props.navigation.navigate("Home")}
               style={StyleWhatOffer.headerTouchable}>
                   <Image style={{width:17,height:17}}
                    source={Images.Cross_Icon}/>
               </TouchableOpacity>
               <Text style={StyleWhatOffer.headerText}>
                  What are you Offring?
               </Text>
           </View>
           <View style={StyleWhatOffer.container}>
                        <FlatList
                            data={Data}
                            renderItem={renderItem}
                            numColumns={2}
                        />
                    </View>
        </View>
    )
}
